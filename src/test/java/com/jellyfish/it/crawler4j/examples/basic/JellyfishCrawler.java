/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jellyfish.it.crawler4j.examples.basic;

import com.jellyfish.it.crawler4j.crawler.Page;
import com.jellyfish.it.crawler4j.crawler.WebCrawler;
import com.jellyfish.it.crawler4j.parser.HtmlParseData;
import com.jellyfish.it.crawler4j.parser.HtmlContentHandler;
import com.jellyfish.it.crawler4j.storage.MysqlCrawler;
import com.jellyfish.it.crawler4j.url.WebURL;

import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.Header;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
/**
 * @author Yasser Ganjisaffar [lastname at gmail dot com]
 */
public class JellyfishCrawler extends WebCrawler {

  private final static Pattern BINARY_FILES_EXTENSIONS =
        Pattern.compile(".*\\.(bmp|gif|jpe?g|png|tiff?|pdf|ico|xaml|pict|rif|pptx?|ps" +
        "|mid|mp2|mp3|mp4|wav|wma|au|aiff|flac|ogg|3gp|aac|amr|au|vox" +
        "|avi|mov|mpe?g|ra?m|m4v|smil|wm?v|swf|aaf|asf|flv|mkv" +
        "|zip|rar|gz|7z|aac|ace|alz|apk|arc|arj|dmg|jar|lzip|lha)" +
        "(\\?.*)?$"); // For url Query parts ( URL?q=... )

  /**
   * You should implement this function to specify whether the given url
   * should be crawled or not (based on your crawling logic).
   */
  @Override
  public boolean shouldVisit(Page page, WebURL url) {
    String href = url.getURL().toLowerCase();

    //return !BINARY_FILES_EXTENSIONS.matcher(href).matches() && href.startsWith("http://careerbuilder.vn/vi") && href.endsWith("jd");
    //return !BINARY_FILES_EXTENSIONS.matcher(href).matches() && href.startsWith("http://careerbuilder.vn/vi") && href.endsWith(".html");
    return !BINARY_FILES_EXTENSIONS.matcher(href).matches() && href.startsWith("http://jobsearch.living.jp/kyujin/");
  }

  /**
   * This function is called when a page is fetched and ready to be processed
   * by your program.
   */
  @Override
  public void visit(Page page) {
    String url = page.getWebURL().getURL();    
    //logger.info("URL: ", url);
    String host = "127.0.0.1";
	String port = "3306";
	String dbName = "crawler";
	String dbUser = "root";
	String dbPwd = "123456";
    MysqlCrawler.createConn(host, port, dbName, dbUser, dbPwd);    
    System.out.println("\n URL visit: " + url);
    
    /*
    if (page.getParseData() instanceof HtmlContentHandler) {
    	HtmlContentHandler htmlParseData2 = (HtmlContentHandler) page.getParseData();
    	String html2 = htmlParseData2.getBodyText();
    	System.out.println("Html: {}" + html2);
    }*/


    if (page.getParseData() instanceof HtmlParseData) {
      HtmlParseData htmlParseData = (HtmlParseData) page.getParseData();
      String text = htmlParseData.getText();
      String html = htmlParseData.getHtml();
      String title = htmlParseData.getTitle();
      
      //String content = htmlParseData.getBodyText();
      Set<WebURL> links = htmlParseData.getOutgoingUrls();

      //logger.debug("Text length: {}", text.length());
      //System.out.println("Text length: {}" + text);
      
      System.out.println("\n Title: {}" + title);
      
      //logger.debug("Html: {}", html);
      //System.out.println("Html: {}" + html);
     
      //logger.debug("Number of outgoing links: {}", links.size());
      //System.out.println("Number of outgoing links: {}" + links.size());
      
      //final String str = "<tag>apple</tag><b>hello</b><tag>orange</tag><tag>pear</tag>";
      System.out.println("\n Matcher: {}" + Arrays.toString(getTagValues(html).toArray())); // Prints [apple, orange, pear]
      
      //MysqlCrawler.getInstance().insertURL(url, title, "");
      
      
      
      // get all email
      //HtmlParseData htmlParseData = (HtmlParseData) page.getParseData();
      //List tr=htmlParseData.getOutgoingUrls();
      /*
      Pattern regex = Pattern.compile("[php]");
      String pageText =  htmlParseData.getText();
      Matcher regexMatcher = regex.matcher(pageText);
      int subEmailCounter = 0;
      int width = 0;
      while (regexMatcher.find()) {
         if((regexMatcher.start()-25 > 0) && (regexMatcher.end()+25 < pageText.length())){
           width=25;
           String[] substr=pageText.substring(regexMatcher.start()-width,regexMatcher.end()+width).split(" ");
            for(int j=0;j<substr.length;j++){
               if(substr[j].contains("@") && (substr[j].contains(".com") || substr[j].contains(".net"))){
                   System.out.println(substr[j]);
                   subEmailCounter++;
               }
             }
              
           } else {
              width=0;
           }
       }
      
      logger.debug("Number of email: ", subEmailCounter);
      System.out.println("Number of email: " + subEmailCounter);
	*/
    }

/*
    Header[] responseHeaders = page.getFetchResponseHeaders();
    if (responseHeaders != null) {
      logger.debug("Response headers:");
      for (Header header : responseHeaders) {
        logger.debug("\t{}: {}", header.getName(), header.getValue());
      }
    }
*/
    logger.debug("=============");
  }
  private static final Pattern TAG_REGEX = Pattern.compile("<div class=\"list_title clearfix\">(.+?)</div>");

  private static List<String> getTagValues(final String str) {
      final List<String> tagValues = new ArrayList<String>();
      final Matcher matcher = TAG_REGEX.matcher(str);
      while (matcher.find()) {
          tagValues.add(matcher.group(1));
      }
      return tagValues;
  }
}