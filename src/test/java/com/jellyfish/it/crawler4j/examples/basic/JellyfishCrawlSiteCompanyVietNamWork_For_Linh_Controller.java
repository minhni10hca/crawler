/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jellyfish.it.crawler4j.examples.basic;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import com.sleepycat.je.txn.LockerFactory;
import com.jellyfish.it.crawler4j.crawler.CrawlConfig;
import com.jellyfish.it.crawler4j.crawler.CrawlController;
import com.jellyfish.it.crawler4j.crawler.Page;
import com.jellyfish.it.crawler4j.crawler.WebCrawler;
import com.jellyfish.it.crawler4j.crawler.exceptions.PageBiggerThanMaxSizeException;
import com.jellyfish.it.crawler4j.fetcher.PageFetchResult;
import com.jellyfish.it.crawler4j.fetcher.PageFetcher;
import com.jellyfish.it.crawler4j.parser.HtmlParseData;
import com.jellyfish.it.crawler4j.robotstxt.RobotstxtConfig;
import com.jellyfish.it.crawler4j.robotstxt.RobotstxtServer;
import com.jellyfish.it.crawler4j.storage.MysqlCrawler;
import com.jellyfish.it.crawler4j.url.WebURL;

import org.apache.http.HttpStatus;
import org.apache.http.impl.EnglishReasonPhraseCatalog;
import org.jsoup.Connection;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

/**
 * @author Yasser Ganjisaffar [lastname at gmail dot com]
 */
public class JellyfishCrawlSiteCompanyVietNamWork_For_Linh_Controller {
	private static Logger logger = LoggerFactory
			.getLogger(JellyfishCrawlSiteCompanyVietNamWork_For_Linh_Controller.class);
	private static PageFetcher pageFetcher;
	public static String pathXmlFile = "./src/test/java/com/jellyfish/it/crawler4j/examples/config/JellyfishCrawlConfigCompany_Linh.xml";
	/* declare read config pagenumber end */
	private static String totalProductSelect = "";
	private static int totalProductIndexBegin = -1;
	private static int totalProductIndexEnd = -1;
	private static String numberProductInPageSelect = "";
	private static String regex = "";
	private static int regexFromIndexBegin = -1;
	private static int regexFromIndexEnd = -1;
	private static int regexToIndexBegin = -1;
	private static int regexToIndexEnd = -1;
	private static int totalProductPosition = -1;
	private static String totalProductSelectPosition = "";
	private static int numberProductInPagePosition = -1;
	private static String numberProductInPageSelectPosition = "";
	private static int timeOut = 5000;
	private static String decimalFormatTotalProduct = "";
	private static String pageNumberSelect = "";
	private static int pageNumberIndexBegin = -1;
	private static int pageNumberIndexEnd = -1;

	/* new - vietnamwork change get data - using WebDriver get data */
	private static String host = "";
	private static String port = "";
	private static String dbName = "";
	private static String dbUser = "";
	private static String dbPwd = "";
	private static String tagConnection = "databaseConnection";

	/* end */

	public static Boolean checkWebSite(URL url) {
		HttpURLConnection connection;
		int code = 0;
		try {
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.connect();
			code = connection.getResponseCode();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (code == 200) {
			return true;
		} else {
			return false;
		}

	}

	public static Boolean readXmlConfigDatabase() {
		try {
			// File fXmlFile = new File(System.getProperty("user.dir")
			// + JellyfishCrawlSiteCompanyController.pathXmlFile);
			File fXmlFile = new File(pathXmlFile);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			org.w3c.dom.Document doc = dBuilder.parse(fXmlFile);
			org.w3c.dom.NodeList nList = doc
					.getElementsByTagName(tagConnection);
			org.w3c.dom.Node nNode = nList.item(0);
			if (nNode.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
				org.w3c.dom.Element eElement = (org.w3c.dom.Element) nNode;
				host = eElement.getElementsByTagName("host").item(0)
						.getTextContent();
				port = eElement.getElementsByTagName("port").item(0)
						.getTextContent();
				dbName = eElement.getElementsByTagName("dbName").item(0)
						.getTextContent();
				dbUser = eElement.getElementsByTagName("dbUser").item(0)
						.getTextContent();
				dbPwd = eElement.getElementsByTagName("dbPassword").item(0)
						.getTextContent();
			}
			return true;

		} catch (Exception e) {
			e.printStackTrace();
			System.out.print("can not load xml file");
			return false;
		}

	}

	public static Boolean tryParseIntByString(String value) {
		try {
			Integer.parseInt(value);
			return true;
		} catch (NumberFormatException ex) {
			return false;
		}
	}

	public static int getPageNumberEnd(String url) {
		try {
			Connection.Response response =

			Jsoup.connect(url)
			// enable for error urls
					.ignoreHttpErrors(true)
					// MAXIMUN TIME
					.timeout(timeOut)
					// This is to prevent producing garbage by attempting to
					// parse a JPEG binary image
					.ignoreContentType(true).execute();

			int status = response.statusCode();
			// after done
			if (status == 200) {
				org.jsoup.nodes.Document doc = response.parse();
				if (!pageNumberSelect.isEmpty()) {
					int pageNumber = -1;
					String strPageNumber = doc.select(pageNumberSelect).text();
					if (pageNumberIndexBegin > -1 && pageNumberIndexEnd > -1) {
						String strSplit = "0";
						for (int i = pageNumberIndexBegin; i <= pageNumberIndexEnd; i++) {
							String ch = String.valueOf(strPageNumber.charAt(i));
							if (tryParseIntByString(ch)) {
								strSplit += ch;
							}
						}
						pageNumber = Integer.parseInt(strSplit);
					} else {
						pageNumber = Integer.parseInt(strPageNumber);
					}
					return pageNumber;

				} else {
					// get pagenumber with total product/numberproduct in page
					String strTotalProduct = "";
					if (totalProductPosition > -1) {
						if (totalProductSelectPosition.isEmpty()) {
							strTotalProduct = doc.select(totalProductSelect)
									.get(totalProductPosition).text();
						} else {
							strTotalProduct = doc.select(totalProductSelect)
									.get(totalProductPosition)
									.select(totalProductSelectPosition).text();
						}
					} else {
						strTotalProduct = doc.select(totalProductSelect).text()
								.toString().trim();
					}
					int totalProduct = -1;
					int i;
					// get totalproduct
					if (totalProductIndexBegin < 0) {
						String strNumberTotalProduct = "0";
						for (i = 0; i < strTotalProduct.length(); i++) {
							String ch = String.valueOf(strTotalProduct
									.charAt(i));
							if (ch.isEmpty() || ch.equals(" ")) {
								break;
							}
							if (tryParseIntByString(ch)) {
								strNumberTotalProduct += ch;
							}
						}
						totalProduct = Integer.parseInt(strNumberTotalProduct);
					} else {
						String strSplit = "0";
						for (i = totalProductIndexBegin; i <= totalProductIndexEnd; i++) {
							String ch = String.valueOf(strTotalProduct
									.charAt(i));
							if (tryParseIntByString(ch)) {
								strSplit += ch;
							}
						}
						totalProduct = Integer.parseInt(strSplit);
					}
					// get number product in page
					int numberPage = -1;
					String strNumberProductInPage = "";
					if (numberProductInPagePosition > -1) {
						if (numberProductInPageSelectPosition.isEmpty()) {
							strNumberProductInPage = doc
									.select(numberProductInPageSelect)
									.get(numberProductInPagePosition).text();
						} else {
							strNumberProductInPage = doc
									.select(numberProductInPageSelect)
									.get(numberProductInPagePosition)
									.select(numberProductInPageSelectPosition)
									.text();
						}
					} else {
						strNumberProductInPage = doc.select(
								numberProductInPageSelect).text();
					}

					if (regexFromIndexEnd < 0 && regexToIndexEnd < 0) {
						String strSplit = "0";
						for (i = 0; i < strNumberProductInPage.length(); i++) {
							String ch = String.valueOf(strNumberProductInPage
									.charAt(i));
							if (ch.isEmpty() || ch.equals(" ")) {
								break;
							}
							if (tryParseIntByString(ch)) {
								strSplit += ch;
							}
						}
						int numberProductInPage = Integer.parseInt(strSplit);
						if (totalProduct > -1 && numberProductInPage > 0) {
							if ((totalProduct % numberProductInPage) == 0) {
								numberPage = totalProduct / numberProductInPage;
							} else if (totalProduct > numberProductInPage) {
								numberPage = totalProduct / numberProductInPage
										+ 1;
							}
						}
						return numberPage;
					} else {
						// String[] arrStrNumberProductInPage =
						// strNumberProductInPage.split(regex);
						// String strNumberProductFrom =
						// arrStrNumberProductInPage[0];
						String strTotalProductReplace = String
								.valueOf(totalProduct);
						if (!decimalFormatTotalProduct.isEmpty()) {
							DecimalFormat formatter = new DecimalFormat(
									decimalFormatTotalProduct);
							strTotalProductReplace = formatter
									.format(totalProduct);
						}
						strNumberProductInPage = strNumberProductInPage
								.replace(strTotalProductReplace, "");
						int numberFrom = -1;
						String strSplitFrom = "0";
						for (i = regexFromIndexBegin; i <= regexFromIndexEnd; i++) {
							String ch = String.valueOf(strNumberProductInPage
									.charAt(i));
							if (tryParseIntByString(ch)) {
								strSplitFrom += ch;
							}
						}
						numberFrom = Integer.parseInt(strSplitFrom);

						// String strNumberProductTo =
						// arrStrNumberProductInPage[1];
						int numberTo = -1;
						String strSplitTo = "0";
						for (i = regexToIndexBegin; i <= regexToIndexEnd; i++) {
							String ch = String.valueOf(strNumberProductInPage
									.charAt(i));
							if (tryParseIntByString(ch)) {
								strSplitTo += ch;
							}
						}
						numberTo = Integer.parseInt(strSplitTo);
						int numberProductInPage = numberTo - numberFrom + 1;
						if (totalProduct > -1 && numberProductInPage > 0) {
							if ((totalProduct % numberProductInPage) == 0) {
								numberPage = totalProduct / numberProductInPage;
							} else if (totalProduct > numberProductInPage) {
								numberPage = totalProduct / numberProductInPage
										+ 1;
							}
						}
						return numberPage;
					}
				}

			} else {
				return -1;
			}

		} catch (SocketTimeoutException se) {

			System.out.println("getContentOnly: SocketTimeoutException");
			System.out.println(se.getMessage());
			return -1;
		}

		catch (Exception e) {

			System.out.println("getContentOnly: Exception");
			e.printStackTrace();
			return -1;
		}

	}

	public static String hashHTML(String input) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(input.getBytes());
			byte[] enc = md.digest();
			String md5Sum = new sun.misc.BASE64Encoder().encode(enc);
			return md5Sum;

		} catch (NoSuchAlgorithmException nsae) {

			System.out.println(nsae.getMessage());
			return null;
		}

	}

	private static void sleep(int microtime) {
		try {
			Thread.sleep(microtime);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static org.jsoup.nodes.Document convertUrlToDocument(String url) {
		try {

			org.jsoup.Connection.Response response = Jsoup.connect(url)
			// enable for error urls
					.ignoreHttpErrors(true).followRedirects(false)
					// MAXIMUN TIME
					.timeout(30000)
					// This is to prevent producing garbage by attempting to
					// parse a JPEG binary image
					.ignoreContentType(true).execute();

			int flag = 0; // 0 for link not redirects 1 if redirects
			Page page = null;
			int status = 0;
			int status_demo = response.statusCode();
			if (response.statusCode() != 200) {
				flag = 1;
				WebURL url2 = new WebURL();
				url2.setURL(url);
				WebCrawler crawler = new WebCrawler();
				page = crawler.getProcessPage(url2);
				status = page.getStatusCode(); // response.statusCode();
				// is redirect
			} else {
				status = response.statusCode();
			}
			// after done
			if (status == 200) {
				Document doc;
				if (flag == 1) {
					HtmlParseData htmlParseData = (HtmlParseData) page
							.getParseData();
					String html = htmlParseData.getHtml();
					doc = Jsoup.parse(html, "UTF-8");
				} else {
					doc = response.parse();
				}
				return doc;
			} else
				return null;
		} catch (SocketTimeoutException se) {
			System.out.println("getContentOnly: SocketTimeoutException");
			System.out.println(se.getMessage());
			return null;
		} catch (Exception e) {
			System.out.println("getContentOnly: Exception");
			e.printStackTrace();
			return null;
		}
	}

	public static void processDataByUrl(String jobUrl, String jobPosted,
			int siteIDXMl) {
		// System.out.println(jobUrl);
		int siteID = siteIDXMl;
		String jobName = "";
		String jobLocation = "";
		String companyName = "";
		String companyAddress = "";
		String companyPhone = "";
		String companyContact = "";
		String companyWebsite = "";
		String job_posted = jobPosted;

		try {

			String[] arrDatePoster = job_posted.split(" ");
			job_posted = arrDatePoster[1];
			org.jsoup.nodes.Document document = convertUrlToDocument(jobUrl);
			jobName = document.select("h1[class=job-title]").text();
			jobLocation = document.select("p[class=work-location] > span > a")
					.text();
			companyName = document.select(".company-name > strong").text();
			companyAddress = document.select(".company-address").text();
			companyContact = document.select(
					"div[class=company-info] > p > strong").text();
			companyWebsite = document.select("span[id=companyprofile]").text();
			
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			Date job_PosterDate = new Date();
			try {
				if (!job_posted.isEmpty()) {
					job_PosterDate = df.parse(job_posted);
				}
			} catch (Exception ex) {
				job_PosterDate = new Date();
			}
			
			if (!jobName.isEmpty()) {
				if (!MysqlCrawler.getInstance()
						.checkJobUrlHRContents_VNW_Linh(jobUrl)) {
					MysqlCrawler.getInstance().insertJFHRContents_VNW_Linh(
							siteID, jobUrl, jobName, jobLocation,
							companyName, companyAddress, companyPhone,
							companyContact, companyWebsite, job_PosterDate, 1);
				} else {
					MysqlCrawler.getInstance().UpdateJFHRContents_VNW_Linh(
							jobUrl, job_PosterDate);
				}
			}
		} catch (Exception ex) {
			System.out.println("\n Ex : " + ex);
		}

	}

	public static String pathChromDriver;

	public static void startSite(String tag_size) throws Exception {

		String crawlStorageFolder = "/crawler4j/storage";
		// int numberOfCrawlers = 1;
		// CrawlConfig config = new CrawlConfig();
		//
		// config.setCrawlStorageFolder(crawlStorageFolder);
		//
		// config.setPolitenessDelay(1000);
		// config.setMaxDepthOfCrawling(1);// ( use -1 for unlimited depth )
		//
		// config.setMaxPagesToFetch(-1);// ( use -1 for unlimited pages )
		//
		// /**
		// * Do you want crawler4j to crawl also binary data ? example: the
		// * contents of pdf, or the metadata of images etc
		// */
		// config.setIncludeBinaryContentInCrawling(false);
		// config.setResumableCrawling(false);
		//
		// config.setUserAgentString("Test");
		// PageFetcher pageFetcher = new PageFetcher(config);
		// RobotstxtConfig robotstxtConfig = new RobotstxtConfig();
		// // by me
		// robotstxtConfig.setEnabled(false);
		// RobotstxtServer robotstxtServer = new
		// RobotstxtServer(robotstxtConfig,
		// pageFetcher);
		// // by me
		// CrawlController controller = new CrawlController(config, pageFetcher,
		// robotstxtServer);

		try{
			// String tag_size = "site101";
			if (!readXmlConfigDatabase())
				return;
			int sizeIDXML = -1;
			String provinceYESNO, linkCrawlerBegin, linkCrawlerPage;
			int pageNumberBegin = 1, pageNumberEnd = -1, pageLoopInit = -1, pageLoop = -1;

			// File fXmlFile = new File(System.getProperty("user.dir")
			// + pathXmlFile);
			File fXmlFile = new File(pathXmlFile);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			org.w3c.dom.Document doc = dBuilder.parse(fXmlFile);
			org.w3c.dom.NodeList nList = doc.getElementsByTagName(tag_size);
			org.w3c.dom.Node nNode = nList.item(0);
			if (nNode.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
				org.w3c.dom.Element eElement = (org.w3c.dom.Element) nNode;
				sizeIDXML = Integer.parseInt(eElement.getAttribute("id"));
				provinceYESNO = eElement.getElementsByTagName("provinceYESNO")
						.item(0).getTextContent();
				if (!provinceYESNO.isEmpty()
						&& provinceYESNO.toUpperCase().equals("YES")) {
					// have sevent province - NOT
					org.w3c.dom.NodeList nListProvince = eElement
							.getElementsByTagName("province");
					for (int index = 0; index < nListProvince.getLength(); index++) {
						pageNumberBegin = 1;
						org.w3c.dom.Element eElementProvince = (org.w3c.dom.Element) nListProvince
								.item(index);
						linkCrawlerBegin = eElementProvince
								.getElementsByTagName("linkCrawlerBegin")
								.item(0).getTextContent();
						if (!linkCrawlerBegin.isEmpty()) {
							System.out.println("visit :" + linkCrawlerBegin);

//							 System.setProperty("webdriver.chrome.driver",
//							 "D:\\lib_java\\chromedriver.exe");
							//
							System.setProperty("webdriver.chrome.driver",
									pathChromDriver); // using winserver

							// System.setProperty("webdriver.chrome.driver","/home/crawler/cronjob/chrome‌​driver");
							// System.setProperty("webdriver.chrome.driver",pathChromDriver);
							WebDriver driver = new ChromeDriver();
							driver.get(linkCrawlerBegin);
							sleep(10000);
							MysqlCrawler.createConn(host, port, dbName, dbUser,
									dbPwd);
							// for (int i = 1; i <= 18; i++) {
							Boolean loop = true;
							// for (; pageNumberBegin <= pageNumberEnd;
							// pageNumberBegin++) {
							while (loop) {
								System.out.println("page:"
										+ String.valueOf(pageNumberBegin));
								if (pageNumberBegin == 1) {
									try {

										driver.findElement(
												By.cssSelector("#pagination-container > div > ul > li.ais-pagination--item.ais-pagination--item__pageundefined.ais-pagination--item__active.active > a"))
												.click();
										sleep(1000);
										List<WebElement> allSuggestions = driver
												.findElements(By
														.cssSelector(".job-item"));
										if (allSuggestions.size() < 1)
											loop = false;
										for (WebElement suggestion : allSuggestions) {
											// get link
											try {
												String url = suggestion
														.findElement(
																By.cssSelector("h3 a"))
														.getAttribute("href");
												String job_posted = suggestion
														.findElement(
																By.cssSelector(".posted"))
														.getText();
												if (!url.isEmpty())
													processDataByUrl(url,
															job_posted,
															sizeIDXML);
												else {
													loop = false;
													break;
												}
											} catch (Exception ex) {
												System.out
														.println("error job IN");
											}

										}
									} catch (Exception ex) {
										System.out.println("error job");
										//loop = false;
									}
								} else {

									try {
										driver.findElement(
												By.cssSelector("#pagination-container > div > ul > li.ais-pagination--item.ais-pagination--item__next > a"))
												.click();
										sleep(1000);
										List<WebElement> allSuggestions = driver
												.findElements(By
														.cssSelector(".job-item"));
										if (allSuggestions.size() < 1)
											loop = false;
										for (WebElement suggestion : allSuggestions) {
											// get link
											try {
												String url = suggestion
														.findElement(
																By.cssSelector("h3 a"))
														.getAttribute("href");
												String job_posted = suggestion
														.findElement(
																By.cssSelector(".posted"))
														.getText();
												if (!url.isEmpty())
													processDataByUrl(url,
															job_posted,
															sizeIDXML);
												else {
													loop = false;
													break;
												}
											} catch (Exception ex) {
												System.out
														.println("error job IN");
											}
										}
									} catch (Exception ex) {
										System.out.println("error job");
										loop = false;
									}
								}
								pageNumberBegin += 1;
							}

							driver.close();
							// }
						}

					}
				} else if (!provinceYESNO.isEmpty()
						&& provinceYESNO.toUpperCase().equals("NO")) {

				}

			}
		} catch (Exception ex) {
			// System.out.print("can't read config xml, review xml file !!");
			System.out.print(ex.getMessage());
		}
	}

	public static void writeLogCrawler(String value) {
		try {
			PrintWriter out = new PrintWriter(new BufferedWriter(
					new FileWriter("Log_Crawler.txt", true)));
			out.println(value);
			out.close();

		} catch (SecurityException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

	public static Boolean checkSiteXml(String tag_sise) {
		try {
			// File fXmlFile = new File(System.getProperty("user.dir")
			// + pathXmlFile);
			File fXmlFile = new File(pathXmlFile);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			org.w3c.dom.Document doc = dBuilder.parse(fXmlFile);
			org.w3c.dom.NodeList nList = doc.getElementsByTagName(tag_sise);
			System.out.print(nList);
			if (nList.getLength() < 1) {
				return false;
			}

		} catch (ParserConfigurationException | SAXException | IOException e) {
			// TODO Auto-generated catch block
			System.out.print("error read xml: " + e.getMessage());
			return false;
		}
		return true;
	}

	public static void updateStatusViewNamWork(int siteIDXML) {
		MysqlCrawler.createConn(host, port, dbName, dbUser, dbPwd);
		Boolean result = MysqlCrawler.getInstance()
				.updateStatusContentsByCrawlDate(siteIDXML);
		String log;
		if (result)
			log = "site8"
					+ " update status = 0 all job yesterday crawler SUCCESS";
		else
			log = "site8" + " update status = 0 all job yesterday crawler FAIL";
		writeLogCrawler(log);
	}

	public static void main(String[] args) throws Exception {
		logger.info("Start...: ");

		// site11
		DateFormat dateTimeFormat = new SimpleDateFormat("dd-MM-yyyy_HH:mm:ss");
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		// site11 - for linh
		String listSize = args[0]; //
		String configXML = "";
		if (args.length > 1) {
			configXML = args[1];// "site9";
			pathChromDriver = args[2];
		}
		if (!configXML.isEmpty()) {
			pathXmlFile = configXML;
		}
		System.out.print(pathXmlFile);
		// String listSize ="site9";
		startSite(listSize);
		// update status yesterday is 0
		//updateStatusViewNamWork(8);

	}
}