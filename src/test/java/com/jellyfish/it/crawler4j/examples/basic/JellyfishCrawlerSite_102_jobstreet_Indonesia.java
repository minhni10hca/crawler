/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jellyfish.it.crawler4j.examples.basic;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.jellyfish.it.crawler4j.crawler.Page;
import com.jellyfish.it.crawler4j.crawler.WebCrawler;
import com.jellyfish.it.crawler4j.parser.HtmlParseData;
import com.jellyfish.it.crawler4j.storage.MysqlCrawler;
import com.jellyfish.it.crawler4j.url.WebURL;

/**
 * @author Yasser Ganjisaffar [lastname at gmail dot com]
 */
public class JellyfishCrawlerSite_102_jobstreet_Indonesia extends WebCrawler {
	private Boolean status_read_xml = false;
	/* Job Img */
	private String jobImgUrl = "";
	private String jobImgQuery = "";
	private String jobImageFormatAttr = "";
	private int jobImagePosition = -1;
	private String JobImageSelectPosition = "";
	/* Job url */
	private String joburl_url = "";
	private String jobUrlQuery = "";
	private String jobUrlFormatAttr = "";
	private int jobUrlPosition = -1;
	private String JobUrlSelectPosition = "";
	/* Job Name */
	private String jobNameQuery = "";
	private String jobNameFormatData = "";
	/* Job Location */
	private String jobLocationQuery = "";
	private String jobLocationFormatData = "";
	/* Job Salary */
	private String JobSalaryQuery = "";
	private String jobSalaryFormatData = "";
	/* Job WorkType */
	private String jobWorkTypeQuery = "";
	private String jobWorkTypeFormatData = "";
	/* Job Detail */
	private String JobDetailQuery = "";
	private String jobDetailFormatData = "";
	/* Job CreatedDate */
	private String JobCreatedDate = "";
	private String JobCreatedDateFormatData = "";
	/* Job Type */
	/* Job Company */
	private String JobCompanyQuery = "";
	private String jobCompanyFormatData = "";
	/* config database */
	private String host = "";
	private String port = "";
	private String dbName = "";
	private String dbUser = "";
	private String dbPwd = "";
	private String tagConnection = "databaseConnection";
	public static String tag_size = "";
	public static int siteIDXML = 102;

	/* query select body */
	private String bodySelect = "";
	private static Boolean status_check_table_logdata = false;

	private final static Pattern BINARY_FILES_EXTENSIONS = Pattern
			.compile(".*\\.(bmp|gif|jpe?g|png|tiff?|pdf|ico|xaml|pict|rif|pptx?|ps"
					+ "|mid|mp2|mp3|mp4|wav|wma|au|aiff|flac|ogg|3gp|aac|amr|au|vox"
					+ "|avi|mov|mpe?g|ra?m|m4v|smil|wm?v|swf|aaf|asf|flv|mkv"
					+ "|zip|rar|gz|7z|aac|ace|alz|apk|arc|arj|dmg|jar|lzip|lha)"
					+ "(\\?.*)?$"); // For url Query parts ( URL?q=... )

	/**
	 * You should implement this function to specify whether the given url
	 * should be crawled or not (based on your crawling logic).
	 */

	public Boolean readXmlConfigDatabase() {
		if (status_read_xml == false) {
			try {
				// File fXmlFile = new File(System.getProperty("user.dir")
				// + JellyfishCrawlSite13Controller.pathXmlFile);
				File fXmlFile = new File(
						JellyfishCrawlSiteMain_Long_Total_Controller.pathXmlFile);
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory
						.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				org.w3c.dom.Document doc = dBuilder.parse(fXmlFile);
				org.w3c.dom.NodeList nList = doc
						.getElementsByTagName(tagConnection);
				org.w3c.dom.Node nNode = nList.item(0);
				if (nNode.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
					org.w3c.dom.Element eElement = (org.w3c.dom.Element) nNode;
					host = eElement.getElementsByTagName("host").item(0)
							.getTextContent();
					port = eElement.getElementsByTagName("port").item(0)
							.getTextContent();
					dbName = eElement.getElementsByTagName("dbName").item(0)
							.getTextContent();
					dbUser = eElement.getElementsByTagName("dbUser").item(0)
							.getTextContent();
					dbPwd = eElement.getElementsByTagName("dbPassword").item(0)
							.getTextContent();
				}
				return true;

			} catch (Exception e) {
				e.printStackTrace();
				System.out.print("can not load xml file");
				return false;
			}
		}
		return true;
	}

	public Boolean ReadXmlConfig() {
		if (status_read_xml == false) {
			try {
				// File fXmlFile = new File(System.getProperty("user.dir")
				// + JellyfishCrawlSite13Controller.pathXmlFile);
				File fXmlFile = new File(
						JellyfishCrawlSiteMain_Long_Total_Controller.pathXmlFile);
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory
						.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				org.w3c.dom.Document doc = dBuilder.parse(fXmlFile);
				org.w3c.dom.NodeList nList = doc
						.getElementsByTagName(this.tag_size);
				org.w3c.dom.Node nNode = nList.item(0);
				if (nNode.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
					org.w3c.dom.Element eElement = (org.w3c.dom.Element) nNode;
					/* body select */
					bodySelect = eElement.getElementsByTagName("bodySelect")
							.item(0).getTextContent();
					/* Job Img */
					jobImgUrl = eElement.getElementsByTagName("JobImage_Url")
							.item(0).getTextContent();
					jobImgQuery = eElement
							.getElementsByTagName("JobImageSelect").item(0)
							.getTextContent();
					jobImageFormatAttr = eElement
							.getElementsByTagName("JobImageFormatAttr").item(0)
							.getTextContent();
					/* Job url */
					joburl_url = eElement.getElementsByTagName("JobUrl_Url")
							.item(0).getTextContent();
					jobUrlQuery = eElement.getElementsByTagName("JobUrlSelect")
							.item(0).getTextContent();
					jobUrlFormatAttr = eElement
							.getElementsByTagName("JobUrlFormatAttr").item(0)
							.getTextContent();
					/* Job name */
					jobNameQuery = eElement
							.getElementsByTagName("JobNameSelect").item(0)
							.getTextContent();
					jobNameFormatData = eElement
							.getElementsByTagName("JobNameFormatData").item(0)
							.getTextContent();
					/* Job Salary */
					JobSalaryQuery = eElement
							.getElementsByTagName("JobSalarySelect").item(0)
							.getTextContent();
					jobSalaryFormatData = eElement
							.getElementsByTagName("JobSalaryFormatData")
							.item(0).getTextContent();

					/* Work Type */
					jobWorkTypeQuery = eElement
							.getElementsByTagName("JobWorkTypeSelect").item(0)
							.getTextContent();
					jobWorkTypeFormatData = eElement
							.getElementsByTagName("JobWorkTypeFormatData")
							.item(0).getTextContent();

					/* Job Detail */
					JobDetailQuery = eElement
							.getElementsByTagName("JobDetailSelect").item(0)
							.getTextContent();
					jobDetailFormatData = eElement
							.getElementsByTagName("JobDetailFormatData")
							.item(0).getTextContent();

					/* Job created date */
					JobCreatedDate = eElement
							.getElementsByTagName("CreatedDateSelect").item(0)
							.getTextContent();
					JobCreatedDateFormatData = eElement
							.getElementsByTagName("CreatedDateFormatData")
							.item(0).getTextContent();
					/* Job location */
					jobLocationQuery = eElement
							.getElementsByTagName("JobLocationSelect").item(0)
							.getTextContent();
					jobLocationFormatData = eElement
							.getElementsByTagName("JobLocationFormatData")
							.item(0).getTextContent();
					/* Job company */
					JobCompanyQuery = eElement
							.getElementsByTagName("JobCompanySelect").item(0)
							.getTextContent();
					jobCompanyFormatData = eElement
							.getElementsByTagName("JobCompanyFormatData")
							.item(0).getTextContent();

				}
				return true;

			} catch (Exception e) {
				e.printStackTrace();
				System.out.print("can not load xml file");
				return false;
			}
		}
		return true;
	}

	public String ConvertStringToNumberString(String salary) {
		String value = "";
		for (int i = 0; i < salary.length(); i++) {
			String ch = String.valueOf(salary.charAt(i));
			if (tryParseIntByString(ch)) {
				value += ch;
			}
		}
		return value;

	}

	@Override
	public boolean shouldVisit(Page page, WebURL url) {
		return false;
		// String href = url.getURL().toLowerCase();

		// return !BINARY_FILES_EXTENSIONS.matcher(href).matches() &&
		// href.startsWith("http://careerbuilder.vn/vi") && href.endsWith("jd");
		// return !BINARY_FILES_EXTENSIONS.matcher(href).matches() &&
		// href.startsWith("http://careerbuilder.vn/vi") &&
		// href.endsWith(".html");
		// return !BINARY_FILES_EXTENSIONS.matcher(href).matches() &&
		// href.startsWith("http://jobsearch.living.jp/kyujin/");
	}

	/**
	 * This function is called when a page is fetched and ready to be processed
	 * by your program.
	 */

	// public org.jsoup.nodes.Document convertUrlToDocument(String url) {
	// try {
	//
	// Connection.Response response =
	//
	// Jsoup.connect(url)
	// // enable for error urls
	// .ignoreHttpErrors(true)
	// // MAXIMUN TIME
	// .timeout(50000)
	// // This is to prevent producing garbage by attempting to
	// // parse a JPEG binary image
	// .ignoreContentType(true).execute();
	//
	// int status = response.statusCode();
	// // after done
	// if (status == 200) {
	// org.jsoup.nodes.Document doc = response.parse();
	// return doc;
	// } else {
	// return null;
	// }
	// } catch (SocketTimeoutException se) {
	//
	// System.out.println("getContentOnly: SocketTimeoutException");
	// System.out.println(se.getMessage());
	// return null;
	// }
	//
	// catch (Exception e) {
	//
	// System.out.println("getContentOnly: Exception");
	// e.printStackTrace();
	// return null;
	// }
	// }

	public Boolean checkSalary(String salary) {
		// true is can get number salary
		// false is can't get number salary
		if (salary.isEmpty())
			return false;
		else {
			try {
				String beginSalary = String.valueOf(salary.charAt(0));
				Integer.parseInt(beginSalary);
				return true;
			} catch (Exception ex) {
				return false;
			}
		}
	}

	public static Boolean tryParseIntByString(String value) {
		try {
			Integer.parseInt(value);
			return true;
		} catch (NumberFormatException ex) {
			return false;
		}
	}

	public int getNumberSalary(String salary) {
		String strNumber = "";
		for (int i = 0; i < salary.length(); i++) {
			String ch = String.valueOf(salary.charAt(i));
			if (tryParseIntByString(ch)) {
				strNumber += ch;
			}
		}
		return Integer.parseInt(strNumber);
	}

	ArrayList<String> listSelector = new ArrayList<String>();

	@Override
	public void visit(Page page) {
		String url = page.getWebURL().getURL();
		// logger.info("URL: ", url);
		if (ReadXmlConfig() && readXmlConfigDatabase()) {
			status_read_xml = true;
		} else {
			return;
		}
		MysqlCrawler.createConn(host, port, dbName, dbUser, dbPwd);
		System.out.println("\n URL visit: " + url);

		if (page.getParseData() instanceof HtmlParseData) {
			HtmlParseData htmlParseData = (HtmlParseData) page.getParseData();
			String text = htmlParseData.getText();
			String html = htmlParseData.getHtml();
			String title = htmlParseData.getTitle();

			Document doc = Jsoup.parse(html, "UTF-8");
			// doc.outputSettings().escapeMode(EscapeMode.xhtml);
			Element body = doc.body();
			Elements listDetail = body.select(bodySelect);
			if (listDetail.size() < 1)
				return;
			Integer i = 0;
			Integer siteID = siteIDXML;
			Integer provinceID = 0;
			String urlDetail, jobName, jobLocation, companyName, companyAddress, companyPhone, companyContact,
					companyWebsite, strJob_posted;
			String job_detail, job_salary;
			String job_salary_from, job_salary_to, jobImageURL;
			int totalJob = 0;
			int totalJobNew = 0;

			// for category_id and city_id
			int category_id = 1, category_id_2 = 0, category_id_3 = 0;
			int city_id = 0, city_id_2 = 0, city_id_3 = 0;
			String salary_type;
			int rank_id = 0;
			String job_work_type;

			for (Element detail : listDetail) {
				i++;
				try {
					urlDetail = "";
					jobName = "";
					jobLocation = "";
					companyName = "";
					companyAddress = "";
					companyPhone = "";
					companyContact = "";
					companyWebsite = "";
					strJob_posted = "";
					job_detail = "";
					job_salary = "Masuk untuk melihat gaji";
					job_salary_from = "";
					job_salary_to = "";
					salary_type = "1";
					rank_id = 10;
					job_work_type = "";
					jobImageURL = "";
					if (!jobUrlQuery.isEmpty()) {
						urlDetail = joburl_url
								+ detail.select(jobUrlQuery).attr(
										jobUrlFormatAttr);
					}

					org.jsoup.nodes.Document documentSelect = MysqlCrawler
							.getInstance().convertUrlToDocument(urlDetail);
					// job name
					if (!jobNameQuery.isEmpty()) {
						if (jobNameFormatData.toUpperCase().equals("TEXT")) {
							jobName = documentSelect.select(jobNameQuery).text();
						} else if (jobNameFormatData.toUpperCase().equals(
								"HTML")) {
							jobName = documentSelect.select(jobNameQuery).html();
						}
					}
					// job location
					if (!jobLocationQuery.isEmpty()) {
						if (jobLocationFormatData.toUpperCase().equals("TEXT")) {
							jobLocation = documentSelect.select(jobLocationQuery)
									.text().replaceAll("Location: ", "").trim();
						} else if (jobLocationFormatData.toUpperCase().equals(
								"HTML")) {
							jobLocation = documentSelect.select(jobLocationQuery)
									.html();
						}
					}
					// job image
					if (!jobImgQuery.isEmpty()) {
						jobImageURL = jobImgUrl
								+ documentSelect.select(jobImgQuery).attr(
										jobImageFormatAttr);
					}
					// job salary
					if (!JobSalaryQuery.isEmpty()) {
						if (jobSalaryFormatData.toUpperCase().equals("TEXT")) {
							job_salary = documentSelect.select(JobSalaryQuery).text();
						} else if (jobSalaryFormatData.toUpperCase().equals(
								"HTML")) {
							job_salary = documentSelect.select(JobSalaryQuery).html();
						}
					}

					if (checkSalary(job_salary)) {
						String[] arrSalary = job_salary.split("–");
						if (arrSalary.length < 2)
							arrSalary = job_salary.split("-");
						if (arrSalary.length > 0) {
							job_salary_from = arrSalary[0];
							int num_from = getNumberSalary(job_salary_from);
							job_salary_from = String.valueOf(num_from) + "";
							job_salary_from = ConvertStringToNumberString(job_salary_from);
						}
						if (arrSalary.length > 1) {
							job_salary_to = arrSalary[1];
							int num_to = getNumberSalary(job_salary_to);
							job_salary_to = String.valueOf(num_to) + "";
							job_salary_to = ConvertStringToNumberString(job_salary_to);
						}

					}

					// work_type;
					if (!jobWorkTypeQuery.isEmpty()) {
						if (jobWorkTypeFormatData.toUpperCase().equals("TEXT")) {
							job_work_type = documentSelect.select(jobWorkTypeQuery).text();
						} else if (jobWorkTypeFormatData.toUpperCase().equals(
								"HTML")) {
							job_work_type = documentSelect.select(jobWorkTypeQuery).html();
						}
					}

					// job detail
					if (!JobDetailQuery.isEmpty()) {
						if (jobDetailFormatData.toUpperCase().equals("TEXT")) {
							job_detail = documentSelect.select(JobDetailQuery).text();
						} else if (jobDetailFormatData.toUpperCase().equals(
								"HTML")) {
							job_detail = documentSelect.select(JobDetailQuery).html();
						}
					}

					// job company
					if (!JobCompanyQuery.isEmpty()) {
						if (jobCompanyFormatData.toUpperCase().equals("TEXT")) {
							companyName = documentSelect.select(JobCompanyQuery).text();
						} else if (jobCompanyFormatData.toUpperCase().equals(
								"HTML")) {
							companyName = documentSelect.select(JobCompanyQuery).html();
						}
					}

					// date created
					String str_date_created = "";
					if (!JobCreatedDate.isEmpty()) {
						if (JobCreatedDateFormatData.toUpperCase().equals(
								"TEXT")) {
							str_date_created = documentSelect.select(JobCreatedDate).text();
						} else if (JobCreatedDateFormatData.toUpperCase()
								.equals("HTML")) {
							str_date_created = documentSelect.select(JobCreatedDate).html();
						}
					}

					strJob_posted = str_date_created;
					DateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
					Date job_PosterDate = new Date();

					try {
						if (!strJob_posted.isEmpty()) {
							job_PosterDate = df.parse(strJob_posted);
						}
					} catch (Exception ex) {
						ex.printStackTrace();
						job_PosterDate = new Date();
					}

					String rank_name = documentSelect
							.select("div.row.job_detail.text_grey2.fw500.mt_6.mb_4 > div:nth-child(2) > p:nth-child(2) > span > span")
							.text().trim();
					if (rank_name.contains("Nhân viên"))
						rank_id = 6;
					else if (rank_name.contains("Trưởng phòng"))
						rank_id = 11;
					else if (rank_name.contains("Trưởng nhóm"))
						rank_id = 11;

					// System.out.println("\n Title : " + jobName);
					if (jobName.isEmpty() || companyName.isEmpty())
						continue;
					if (!MysqlCrawler.getInstance()
							.checkJobUrlHRContents_VNW_001(urlDetail)) {
						totalJobNew += 1;
						MysqlCrawler.getInstance().insertJFHRContents_Long(
								siteID,
								urlDetail,
								jobName,
								jobLocation,
								companyName,
								companyAddress,
								companyPhone,
								companyContact,
								companyWebsite,
								job_PosterDate,
								1,
								job_salary,
								job_detail,
								category_id,
								category_id_2,
								category_id_3,
								city_id,
								city_id_2,
								city_id_3,
								job_salary_from,
								job_salary_to,
								salary_type,
								rank_id);

					} else {
						MysqlCrawler.getInstance().UpdateJFHRContents_Long(
								urlDetail, job_PosterDate);
					}
					totalJob += 1;

//					if (totalJob == 1) {
//						System.out.println(
//								siteID + " \n" +
//										urlDetail + " \n" +
//										jobImageURL + " \n" +
//										jobName + " \n" +
//										jobLocation + " \n" +
//										companyName + " \n" +
//										job_PosterDate + " \n" +
//										job_salary + " \n" +
//										job_detail + " \n" +
//										job_salary_from + " \n" +
//										job_salary_to + " \n" +
//										salary_type + " \n" +
//										rank_id);
//					}

					// System.exit(1);
				} catch (Exception ex) {
					// System.out.println("\n Fail I : " + i);
					// System.out.println("\n Ex : " + ex);
					logger.error(ex.getMessage());
					// insert log error
					MysqlCrawler.getInstance().insertLogError(
							siteID,
							"File: " + ex.getStackTrace()[1].getFileName()
									+ " Line: "
									+ ex.getStackTrace()[1].getLineNumber(),
							ex.getMessage());
				}

			}
			// System.out.println("\n Total Div: --" + listDetail.size());
			JellyfishCrawlSiteMain_Long_Total_Controller.totalJob += totalJob;
			JellyfishCrawlSiteMain_Long_Total_Controller.totalJobNew += totalJobNew;
		}

	}
}