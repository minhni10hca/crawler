package com.jellyfish.it.crawler4j.examples.basic;

public class JellyfishCrawlConfig {
	public static String pathXmlFile = "/src/test/java/com/jellyfish/it/crawler4j/examples/config/JellyfishCrawlConfig.xml";
	public static String tag_size_13 = "site13";
	public static String tag_size_17 = "site17";
	public static String tag_size_16 = "site16";
	public static String tag_size_9 = "site9";
	public static String tag_size_6 = "site6";
	public static String tag_size_15 = "site15";
}
